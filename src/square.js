import { useEffect, useState } from "react";
const initialState = ['', '', '', '', '', '', '', '', ''];
const SquareBoard = () => {

    const [gameState, updateGameState] = useState(initialState);
    const [isPlay, updatePlay] = useState(false);
    const [winner, upadteWinner] = useState('');
    const squareClick = (idx) => {
        if (winner) {

        } else {


            let str = Array.from(gameState);
            if (str[idx]) {
                return;
            }
            console.log(str[idx]);
            str[idx] = isPlay ? 'X' : '0';
            console.log(str[idx]);
            updateGameState(str);
            updatePlay(!isPlay);
        }
    }

    const checkWinner = (() => {
        const lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6],
        ];
        for (let i = 0; i < lines.length; i++) {
            const [a, b, c] = lines[i];
            if (gameState[a] === gameState[b] && gameState[a] === gameState[c]) {
                return gameState[a];
            }
        }
        return null;
    })


    // const calculatWinner = () => {
    //     const win = checkWinner();
    //     if (win === null) {
    //         upadteWinner ('');
    //     }
    //     else {
    //         console.log(winner);
    //         upadteWinner(win);
    //         console.log(winner);
    //         updateGameState (initialState);
    //     }
    // };

    const newGame = (() => {
        updateGameState(initialState);
        upadteWinner('');
    })


    useEffect(() => {

        // const win = checkWinner();
        // if (win) {
        //     upadteWinner(win);
        //     updateGameState(initialState);
        //     // alert(win + 'wins the game');
        // }
    }, [gameState])

    useEffect(() => {
        let win = checkWinner();
        upadteWinner(win);
    });



    return (
        <div className="parent">
            <div className="container">
                <div className="section1">
                    <div className="item1" onClick={() => {
                        squareClick(0);
                    }}>{gameState[0]}</div>
                    <div className="item2" onClick={() => {
                        squareClick(1);
                    }}>{gameState[1]}</div>
                    <div className="item3" onClick={() => {
                        squareClick(2);
                    }}>{gameState[2]}</div>
                </div>
                <div className="section2">
                    <div className="item4" onClick={() => {
                        squareClick(3);
                    }}>{gameState[3]}</div>
                    <div className="item5" onClick={() => {
                        squareClick(4);
                    }}>{gameState[4]}</div>
                    <div className="item6" onClick={() => {
                        squareClick(5);
                    }}>{gameState[5]}</div>
                </div><div className="section3">
                    <div className="item7" onClick={() => {
                        squareClick(6);
                    }}>{gameState[6]}</div>
                    <div className="item8" onClick={() => {
                        squareClick(7);
                    }}>{gameState[7]}</div>
                    <div className="item9" onClick={() => {
                        squareClick(8);
                    }}>{gameState[8]}</div>
                </div>
            </div>
            <div id="winner-looser">Winner is {winner}</div>
            <div className="clear">
                <button onClick={() => {
                    newGame();
                }}>Start New Game</button>
            </div>
        </div>
    );
}

export default SquareBoard;