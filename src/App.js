import logo from './logo.svg';
import './index.css'
import Heading from './heading';
import SquareBoard from './square';

function App() {
  return (
    <div className="App">
      <Heading />
      <SquareBoard />
    </div>
  );
}

export default App;
